module ddr3_rw_top(
    input sys_clk, //????
    input sys_rst_n, //??,???
    // DDR3
    inout [15:0] DDR3_dq, //DDR3??
    inout [1:0] DDR3_dqs_n, //DDR3 dqs?
    inout [1:0] DDR3_dqs_p, //DDR3 dqs?
    output [13:0] DDR3_addr, //DDR3 ??
    output [2:0] DDR3_ba, //DDR3 banck ??
    output DDR3_ras_n, //DDR3 ???
    output DDR3_cas_n, //DDR3 ???
    output DDR3_we_n, //DDR3 ????
    output DDR3_reset_n, //DDR3 ??
    output [0:0] DDR3_ck_p, //DDR3 ???
    output [0:0] DDR3_ck_n, //DDR3 ???
    output [0:0] DDR3_cke, //DDR3 ????
    output [0:0] DDR3_cs_n, //DDR3 ??
    output [1:0] DDR3_dm, //DDR3_dm
    output [0:0] DDR3_odt, //DDR3_odt
    //??
    output led //??????
);
wire ui_clk ; //????
wire error_flag; //??????
wire [27:0] app_addr; //DDR3 ??
wire [2:0] app_cmd; //??????
wire app_en; //MIG IP?????
wire app_rdy; //MIG IP???????
wire [127:0] app_rd_data; //?????
wire app_rd_data_end; //?????????????
wire app_rd_data_valid; //?????
wire [127:0] app_wdf_data; //?????
wire app_wdf_end; //?????????????
wire [15:0] app_wdf_mask; //?????
wire app_wdf_rdy; //????????
wire app_sr_active; //??
wire app_ref_ack; //????
wire app_zq_ack; //ZQ ????
wire app_wdf_wren; //DDR3 ???
wire locked; //?????????
wire clk_ref_i; //DDR3????
wire sys_clk_i; //MIG IP?????
wire clk_200m; //200M??
wire ui_clk_sync_rst; //??????
wire init_calib_complete; //??????
wire [20:0] rd_cnt; //???????
wire [1 :0] state; //?????
wire [23:0] rd_addr_cnt; //????????
wire [23:0] wr_addr_cnt; //????????

//????
DDR3_rw u_DDR3_rw(
    .ui_clk (ui_clk),
    .ui_clk_sync_rst (ui_clk_sync_rst),
    .init_calib_complete (init_calib_complete),
    .app_rdy (app_rdy),
    .app_wdf_rdy (app_wdf_rdy),
    .app_rd_data_valid (app_rd_data_valid),
    .app_rd_data (app_rd_data),
    .app_addr (app_addr),
    .app_en (app_en),
    .app_wdf_wren (app_wdf_wren),
    .app_wdf_end (app_wdf_end),
    .app_cmd (app_cmd),
    .app_wdf_data (app_wdf_data),
    .state (state),
    .rd_addr_cnt (rd_addr_cnt),
    .wr_addr_cnt (wr_addr_cnt),
    .rd_cnt (rd_cnt),
    .error_flag (error_flag),
    .led (led)
);

//MIG IP???
mig_7series_0 u_mig_7series_0 (
    // Memory interface ports
    .DDR3_addr (DDR3_addr), // output [13:0]DDR3_addr
    .DDR3_ba (DDR3_ba), // output [2:0] DDR3_ba
    .DDR3_cas_n (DDR3_cas_n), // output DDR3_cas_n
    .DDR3_ck_n (DDR3_ck_n), // output [0:0] DDR3_ck_n
    .DDR3_ck_p (DDR3_ck_p), // output [0:0] DDR3_ck_p
    .DDR3_cke (DDR3_cke), // output [0:0] DDR3_cke
    .DDR3_ras_n (DDR3_ras_n), // output DDR3_ras_n
    .DDR3_reset_n (DDR3_reset_n), // output DDR3_reset_n
    .DDR3_we_n (DDR3_we_n), // output DDR3_we_n
    .DDR3_dq (DDR3_dq), // inout [15:0] DDR3_dq
    .DDR3_dqs_n (DDR3_dqs_n), // inout [1:0] DDR3_dqs_n
    .DDR3_dqs_p (DDR3_dqs_p), // inout [1:0] DDR3_dqs_p
    .init_calib_complete (init_calib_complete),
    // init_calib_complete
    .DDR3_cs_n (DDR3_cs_n), // output [0:0] DDR3_cs_n
    .DDR3_dm (DDR3_dm), // output [1:0] DDR3_dm
    .DDR3_odt (DDR3_odt), // output [0:0] DDR3_odt
    // Application interface ports
    .app_addr (app_addr), // input [27:0] app_addr
    .app_cmd (app_cmd), // input [2:0] app_cmd
    .app_en (app_en), // input app_en
    .app_wdf_data (app_wdf_data), // input [127:0]app_wdf_data
    .app_wdf_end (app_wdf_end), // input app_wdf_end
    .app_wdf_wren (app_wdf_wren), // input app_wdf_wren
    .app_rd_data (app_rd_data), // output [127:0]app_rd_data
    .app_rd_data_end (app_rd_data_end),
    // output app_rd_data_end
    .app_rd_data_valid (app_rd_data_valid),
    // output app_rd_data_valid
    .app_rdy (app_rdy), // output app_rdy
    .app_wdf_rdy (app_wdf_rdy), // output app_wdf_rdy
    .app_sr_req (), // input app_sr_req
    .app_ref_req (), // input app_ref_req
    .app_zq_req (), // input app_zq_req
    .app_sr_active (app_sr_active),// output app_sr_active
    .app_ref_ack (app_ref_ack), // output app_ref_ack
    .app_zq_ack (app_zq_ack), // output app_zq_ack
    .ui_clk (ui_clk), // output ui_clk
    .ui_clk_sync_rst (ui_clk_sync_rst),
    // output ui_clk_sync_rst
    .app_wdf_mask (31'b0), // input [15:0] app_wdf_mask
    // System Clock Ports
    .sys_clk_i (clk_200),
    // Reference Clock Ports
    .clk_ref_i (clk_200),
    .sys_rst (sys_rst_n) // input sys_rst
);

//????
clk_wiz_0 u_clk_wiz_0
(
    // Clock out ports
    .clk_out1(clk_200), // output clk_out1
    // Status and control signals
    .reset(1'b0), // input resetn
    .locked(locked), // output locked
    // Clock in ports
    .clk_in1(sys_clk)
); // input clk_in1

endmodule

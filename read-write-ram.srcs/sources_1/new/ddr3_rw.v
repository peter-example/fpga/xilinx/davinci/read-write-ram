module DDR3_rw (
    input ui_clk,                       //User clock
    input ui_clk_sync_rst,         //Reset
    input init_calib_complete,   //DDR3 initialization complete
    input app_rdy,                      //MIG command reception ready for Peugeot
    input app_wdf_rdy,              //MIG data reception ready7
    input app_rd_data_valid,    //Read data valid
    input [127:0] app_rd_data, //User read data
    output reg [27:0] app_addr, //DDR3 address
    output app_en,                      //MIG IP send command enable
    output app_wdf_wren,            //User write data enable
    output app_wdf_end,             //Burst write the last data of the current clock
    output [2:0] app_cmd,           //MIG IP core operation command, read or write
    output reg [127:0] app_wdf_data, //User write data
    output reg [1 :0] state,            //Read and write state
    output reg [23:0] rd_addr_cnt, //User read address count
    output reg [23:0] wr_addr_cnt, //User write address count
    output reg [20:0] rd_cnt,           //Actual read address flag
    output reg error_flag,              //Read and write error flag
    output reg led                          //Read and write test result indicator
);
    
    //parameter define
    parameter TEST_LENGTH = 10;
    parameter L_TIME = 25'd25_000_000;
    parameter IDLE = 2'd0; //????
    parameter WRITE = 2'd1; //???
    parameter WAIT = 2'd2; //???????
    parameter READ = 2'd3; //???
    
    //reg define
    reg [24:0] led_cnt; //led??
    
    //wire define
    wire error; //??????
    wire rst_n; //??????
    
    //*****************************************************
    //** main code
    //*****************************************************
    
    assign rst_n = ~ui_clk_sync_rst;
    //???????????????????????????
    assign error = (app_rd_data_valid && (rd_cnt != app_rd_data));
    
    //????MIG IP ?????????????,???????????????????????
    assign app_en = ((state == WRITE && (app_rdy && app_wdf_rdy))
    ||(state == READ && app_rdy)) ? 1'b1:1'b0;
    
    //????,?????????????????????
    assign app_wdf_wren = (state == WRITE && (app_rdy && app_wdf_rdy)) ? 1'b1:1'b0;
    
    //??DDR3??????????????4:1??????8????????
    assign app_wdf_end = app_wdf_wren;
    
    //??????????1?????????0
    assign app_cmd = (state == READ) ? 3'd1 :3'd0;
    
    //DDR3??????
    always @(posedge ui_clk or negedge rst_n) begin
        if((~rst_n)||(error_flag)) begin
            state <= IDLE;
            app_wdf_data <= 128'd0;
            wr_addr_cnt <= 24'd0;
            rd_addr_cnt <= 24'd0;
            app_addr <= 28'd0;
        end
        else if(init_calib_complete)begin //MIG IP??????
            case(state)
                IDLE:begin
                    state <= WRITE;
                    app_wdf_data <= 128'd0;
                    wr_addr_cnt <= 24'd0;
                    rd_addr_cnt <= 24'd0;
                    app_addr <= 28'd0;
                end
                WRITE:begin
                    if(wr_addr_cnt == TEST_LENGTH - 1 &&(app_rdy && app_wdf_rdy))
                        state <= WAIT; //?????????????
                    else if(app_rdy && app_wdf_rdy)begin //?????
                        app_wdf_data <= app_wdf_data + 1; //?????
                        wr_addr_cnt <= wr_addr_cnt + 1; //?????
                        app_addr <= app_addr + 8; //DDR3 ???8
                    end
                    else begin //????????????
                        app_wdf_data <= app_wdf_data;
                        wr_addr_cnt <= wr_addr_cnt;
                        app_addr <= app_addr;
                    end
                end
                WAIT:begin
                    state <= READ; //???????????
                    rd_addr_cnt <= 24'd0; //?????
                    app_addr <= 28'd0; //DDR3????0??
                end
                READ:begin //?????????
                    if(rd_addr_cnt == TEST_LENGTH - 1 && app_rdy)
                        state <= IDLE; //???????
                    else if(app_rdy)begin //?MIG?????,????
                        rd_addr_cnt <= rd_addr_cnt + 1'd1; //????????
                        app_addr <= app_addr + 8; //DDR3???8
                    end
                    else begin //?MIG????,?????
                        rd_addr_cnt <= rd_addr_cnt;
                        app_addr <= app_addr;
                    end
                end
                default:begin
                    state <= IDLE;
                    app_wdf_data <= 128'd0;
                    wr_addr_cnt <= 24'd0;
                    rd_addr_cnt <= 24'd0;
                    app_addr <= 28'd0;
                end
            endcase
        end
    end
    
    //?DDR3???????????
    always @(posedge ui_clk or negedge rst_n) begin
        if(~rst_n)
            rd_cnt <= 0; //?????????????????????0
        else if(app_rd_data_valid && rd_cnt == TEST_LENGTH - 1)
            rd_cnt <= 0; //????????????????1
        else if (app_rd_data_valid )
            rd_cnt <= rd_cnt + 1;
    end
    
    //???????
    always @(posedge ui_clk or negedge rst_n) begin
        if(~rst_n)
            error_flag <= 0;
        else if(error)
            error_flag <= 1;
    end
    
    //led??????
    always @(posedge ui_clk or negedge rst_n) begin
        if((~rst_n) || (~init_calib_complete )) begin
            led_cnt <= 25'd0;
            led <= 1'b0;
        end
        else begin
            if(~error_flag) //??????
                led <= 1'b1; //led???
            else begin //??????
                led_cnt <= led_cnt + 25'd1;
                if(led_cnt == L_TIME - 1'b1) begin
                    led_cnt <= 25'd0;
                    led <= ~led; //led???
                end
            end
        end
    end

endmodule